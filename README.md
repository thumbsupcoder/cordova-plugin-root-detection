---
title: RootDetection
description: Cordova Root Detection Plugin.
---
<!---
# license: 
The MIT License (MIT)

Copyright (c) 2018 Thumbsupcoder

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
-->

# cordova-plugin-root-detection

This plugin defines a global `plugin.rootDetection` object, which provides an API for identifying whether device has been rooted or jailbroken.

Although the object is attached to the global scoped `plugin.rootDetection`, it is not available until after the `deviceready` event.

    document.addEventListener("deviceready", onDeviceReady, false);
    function onDeviceReady() {
        console.log(plugin.rootDetection);
    }


## Installation

This requires cordova 6.0+

    cordova plugin add https://bitbucket.org/thumbsupcoder/cordova-plugin-root-detection.git


## How to Contribute

Contributors are welcome! And we need your contributions to keep the project moving forward. You can [report bugs](https://bitbucket.org/thumbsupcoder/cordova-plugin-root-detection/issues/new), improve the documentation, or [contribute code](https://bitbucket.org/thumbsupcoder/cordova-plugin-root-detection/pull-requests/).

⚠ **Found an issue?** File it on [Bitbucket issue tracker](https://bitbucket.org/thumbsupcoder/cordova-plugin-root-detection/issues/new).

**Have a solution?** Send a [Pull Request](https://bitbucket.org/thumbsupcoder/cordova-plugin-root-detection/pull-requests/).

---

## Usage

### plugin.rootDetection.isCompromised(successCallback, errorCallback)
This plugin provides an API for identifying whether device has been rooted or jailbroken.  `Boolean` value indicating whether the device has compromised or not will be returned in the callbacks method provided.

__Supported Platforms__

- Android
- iOS

**Example**  
```js
plugin.rootDetection.isCompromised(onSuccess, onError);

function onSuccess(compromised) {
    if (compromised) {
        alert("Device has been rooted/jailbroken");
    }
    else {
        console.log('Device has not rooted or jailbroken');
    }
}

function onError(message) {
    alert('Something went wrong, unable to figure out rooted configurations');
}
```
